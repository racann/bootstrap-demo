<?php

$first_name = $_POST['fist_name'];
$last_name = $_POST['last_name'];
$position = $_POST['position'];
$pay = $_POST['pay'];

$file = fopen('data/employees.csv', 'a');
flock($file, LOCK_EX);

if (!$file) {
    echo "Sorry could not write to file";
    exit;
}

$data = $first_name . ", " . $last_name . ", " . $position . ", " . $pay;

fwrite($file, $data, strlen($data));
flock($file, LOCK_UN);
fclose($file);

echo "Employee Added";

?>