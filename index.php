<!doctype html>
<html lang="en">
  <?php include('head.php'); ?>
  <body>
    <div class="container">
      <h1>Bootstrap Demo</h1>
        <?php include('header.php');?>

        <div class="row">     
          <div class="jumbotron w-100">
            <h1 class="display-4">Bootstrap and Jquery Demo</h1>
            <p class="lead">Dynamic Web Demo</p>
            <hr class="my-4">
            <p>Examples use ChartsJs, Datatables, Bootstrap and Jquery</p>
            <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p>
        </div>

        </div><!--End Row -->

        <div class="row">
          <div class="col">
            <a href="charts.php" class="nounderline">
          <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">ChartJs</h5>
             
              <p class="card-text">ChartsJs is a Javascript library tha easily creates many kinds of charts.</p>
            </div>
          </div>
            </a>
          </div>
          <div class="col">
            <a href="datatables.php" class="nounderline" >
          <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Datatables</h5>
            
              <p class="card-text">Javascript library for making good looking tables with sorting and searching features.</p>
            </div>
          </div>
            </a>
          </div>
          <div class="col">
            <a href="animations.php" class="nounderline">
          <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Jquery UI</h5>
             
              <p class="card-text">jQuery UI is a set of effects, widgets, and themes built on top of jQuery.</p>
              </div>
          </div>
            </a>
        </div><!--end row-->

        <?php include('footer.php');?>
       
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>