<!doctype html>
<html lang="en">
  <?php include('head.php'); ?>
  <body>
    <div class="container" id="rootContainer">
      <h1>Bootstrap Demo</h1>
        <?php include('header.php');?>
        
        
          <h2>Fading Animations</h2>
          <br>
          <p id="fadetext">Jquery can be used for fading animations. Click the button to see...</p>
          
          <br>
          <div class="btn btn-primary" id="fadebutton">Toggle Fade</div>
          
        
        

        <?php include('footer.php');?>
       
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  <script>
  $( function() {
    $("#fadebutton").click(function(){
      $("#fadetext").append("appended some text...");
    })
  } );
  </script>
  </body>
</html>